import { ReactMic } from 'react-mic';
import React from 'react';
import openSocket from 'socket.io-client'

var socket = null;

let flag = false;


export class AudioRecorder extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          record: false,
          
        };

        this.startRecording =  this.startRecording.bind(this);
        this.stopRecording = this.stopRecording.bind(this);
        
    
      }

    
      onStop(recordedBlob) {
        console.log('recordedBlob is: ', recordedBlob);
      }

      initializeRecorder(stream){

     //   if(socket == null){
     //       console.log('socket is null')
     //   }
     //   else{

       // socket = openSocket('http://localhost:5000');
        console.log('chunk of real-time data is: ', stream);

        var chunk = {
            content: stream,
            demo: 'demo',
            lang: 'kannada',
            token: 'token'
          }
  
          socket.emit('audio', chunk);

        socket.on('recieved', (data) => {

          console.log(data);

        });
  //  }
    

    }
    
    recorderProcess(e) {
        var left = e.inputBuffer.getChannelData(0);
        //socket.emit('audio', left);
        //socket.emit('audio', convertFloat32ToInt16(left));
    }

    onData(recordedBlob) {
      
      

    }

    startRecording = () => {
    
        this.setState({
            record: true
          });
        if(socket == null){
            socket = openSocket('http://localhost:5000');
            socket.on('connect', function() {
                socket.emit('conn_succ', 'editor is connected');
                
            });

            socket.on('response', function(msg){
                console.log('back from server', msg);
            });

        }
        else {
            socket.disconnect();
            
        }

        

      }
    
      stopRecording = () => {

        

        

        this.setState({
          record: false
        });
      
        socket.disconnect();
      }


    render(){

        return (

            <div className="mic">
                <ReactMic
                  record={this.state.record}
                  className="sound-wave"
                  onStop={this.onStop}
                  onData={this.initializeRecorder}
                  strokeColor="#000000"
                  backgroundColor="#FF4081"
                   />
        <button onClick={this.startRecording} type="button">Start</button>
        <button onClick={this.stopRecording} type="button">Stop</button>
      </div>
        );

    }

}

