/* eslint-env browser */
import React from 'react';
import Resampler from 'resampler'
import Recorder from 'recorderjs'
const audioType = 'audio/*';

var mediaStream = null;

var audio_context = null;



class RecordingAPI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recording: false,
      audios: [],
    };
  }

  initializeRecorder(stream){
    // https://stackoverflow.com/a/42360902/466693
    mediaStream = stream;

    
    var inputBuffer = function(buffer){
        let l = buffer.length;
        let buf = new Float32Array(l);
        while (l--) {
        buf[l] = Math.min(1, buffer[l])*0x7FFF;
    }
        console.log(buf);
        return buf.buffer;
    };

   

    // get sample rate
    audio_context = new AudioContext();


     var sampleRate = audio_context.sampleRate;
    //console.log('<sample_rate>', sampleRate);
   // socket.emit('sample_rate', sampleRate);

    var audioInput = audio_context.createMediaStreamSource(stream);

    console.log("Created media stream.");

    var bufferSize = 4096;
    // record only 1 channel
    var recorder = audio_context.createScriptProcessor(bufferSize);

  //  var rec = new Recorder(recorder, { numChannels: 1, sampleRate: 16000});
    // specify the processing function

     var iBuffer = inputBuffer(mediaStream);
    var resampler = new Resampler(44100, 16000, iBuffer);

    recorder.onaudioprocess = function(e){
        var left = e.inputBuffer.getChannelData(0);
     //   console.log(typeof(left));
        var resampled = resampler.resampler(left);
        console.log("resampled" ,resampled)
    };
    // connect stream to our recorder
    audioInput.connect(recorder);
    // connect our recorder to the previous destination
    recorder.connect(audio_context.destination);      
 }
 
 convertToFloat32 = (buffer) => {
    
 }

 convertFloat32ToInt16(buffer) {
   l = buffer.length;
   buf = new Int16Array(l);
   while (l--) {
     buf[l] = Math.min(1, buffer[l])*0x7FFF;
   }
   console.log(buf);
   return buf.buffer;
 }


  startRecording(e) {
    
    e.preventDefault();
    navigator.getUserMedia({audio: true, channelCount: {ideal: 1}, sampleRate: 16000}, this.initializeRecorder, function(a, b, c){
        console.log(a,b,c);
    });

    

    //console.log('<sample-rate>',audio_context.sampleRate);
    
    
  }

  stopRecording(e) {
    e.preventDefault();
    // stop the recorder
    mediaStream.getAudioTracks()[0].stop();
    audio_context.close();
    // say that we're not recording
    this.setState({recording: false});
    // save the video to memory
   // this.saveAudio();
  }

  saveAudio() {
    // convert saved chunks to blob
    const blob = new Blob(this.chunks, {type: audioType});
    // generate video url from blob
    const audioURL = window.URL.createObjectURL(blob);
    // append videoURL to list of saved videos for rendering
    const audios = this.state.audios.concat([audioURL]);
    this.setState({audios});
  }

  deleteAudio(audioURL) {
    // filter out current videoURL from the list of saved videos
    const audios = this.state.audios.filter(a => a !== audioURL);
    this.setState({audios});
  }

  render() {
    const {recording, audios} = this.state;

    return (
      <div className="camera">
        <audio


          style={{width: 400}}
          ref={a => {
            this.audio = a;
          }}>
         <p>Audio stream not available. </p>
        </audio>
        <div>
          <button onClick={e => this.startRecording(e)}>Record</button>
          <button onClick={e => this.stopRecording(e)}>Stop</button>
        </div>
        <div>
          <h3>Recorded audios:</h3>
          {audios.map((audioURL, i) => (
            <div key={`audio_${i}`}>
              <audio controls style={{width: 200}} src={audioURL}   />
              <div>
                <button onClick={() => this.deleteAudio(audioURL)}>Delete</button>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
export default RecordingAPI