import React from 'react';
require('../css/App.css');
import  AudioRecorder  from './AudioRecorder';
import  RecorderAPI  from './RecorderAPI';


export default class App extends React.Component {
  render () {
    return (
      <div className="App">
        <div className="headline">
          <h1>Gnani Editor</h1>
      </div>

      <RecorderAPI />
        
        </div>);
  }
}