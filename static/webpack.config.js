const webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');


const config = {
    entry: [ 'babel-polyfill',__dirname + '/js/index.jsx'],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.css']
    },
    module: {
        rules: [
            {
                test: /\.jsx?/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'react', 'es2015'],
                        plugins: ['transform-class-properties', 
                                  'transform-regenerator']

                    },
                    
                }
                
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                })
            },
        ]
    },
    plugins: [
        new ExtractTextPlugin('styles.css'),
    ]
};
module.exports = config;
