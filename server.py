from flask import Flask, render_template,session
from flask_socketio import SocketIO, emit, join_room
from flask_cors import CORS
import grpc

import   stt_pb2

import stt_pb2_grpc
import queue

import scipy.io.wavfile

import numpy as np

app = Flask(__name__, static_url_path='')


app.config['SECRET_KEY'] = 'development key'
socket = SocketIO(app)
CORS(app)


_TIMEOUT_SECONDS = 10
_TIMEOUT_SECONDS_STREAM = 1000

@app.route("/")
def index():
    return app.send_static_file('index.html')

@socket.on('conn_succ')
def conn(msg):
    print(msg)
    emit('response', 'flask response')

@socket.on('connect')
def connection_made():

    #service = createService("voicenoteshindi.gnani.ai", "9080")

    session['audio'] = []
    session['audioQ']=queue.Queue()

    emit('connect_succ', 'User is connected')



def createService(ipaddr, port):
        channel = grpc.insecure_channel(ipaddr + ":" + str(port))
        return stt_pb2_grpc.ListenerStub(channel)




def request(chunkIterator):
            for each in chunkIterator:
                yield stt_pb2.SpeechChunk(content=each)




@socket.on('audio')
def audio_received(chunk):

    print('==============================================================================================')
    print('Audio chunk being received.............')
    #print(chunk['content'])
    session['audio']+=chunk['content']
    session['audioQ'].put(chunk['content'])
    print('==============================================================================================')






@socket.on('disconnect')
def disconnect_server():
    print('Socket connection removed')
    service = createService("voicenotestelugu.gnani.ai", "9080")
    print(type(session['audio']))
    chunk_list=session['audio']
    iter_list=iter(session['audio'])

    sample_rate = 32000
    my_audio = np.array(session['audio'], np.float32)
    sindata = np.sin(my_audio)
    scaled = np.round(32767 * sindata)
    newdata = scaled.astype(np.int16)
    scipy.io.wavfile.write('out.wav', sample_rate, newdata)


    def request(chunkIterator):
        for each in chunkIterator:
            yield stt_pb2.SpeechChunk(content=each)

    responses = service.DoSpeechToText(request(iter(session['audioQ'].get)), _TIMEOUT_SECONDS_STREAM)
    for response in responses:
        print(response.transcript.encode("UTF-8"))
        










@socket.on('disconnect_req')
def disconnect_request():
    print('Socket connection removed')



@app.route("/hello")
def hello():
    return ('Hello World!')

if __name__ == "__main__":
    app.secret_key = "development key"
    socket.run(app,host='0.0.0.0', port='65080',debug=True);